/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.services;

/**
 *
 * @author adrie
 */
public class Calculatrice {
    public double addition(double nombreUn, double nombreDeux){
        return nombreUn + nombreDeux;
    }
    
    public double soustraction(double nombreUn,double nombreDeux){
        return nombreUn - nombreDeux;
    }
    
    public double multiplication(double nombreUn, double nombreDeux){
        return nombreUn * nombreDeux;
    }
    
    public double division(double numerateur, double diviseur){
        if (diviseur != 0){
            return numerateur / diviseur;
        } else {
            throw new ArithmeticException("Math Error!!");
        }
    }
    
    public double moyenne(int[] tableaux){
        if (tableaux != null){
            
            int somme = 0;
        
        for (int nombre : tableaux){
            somme += nombre;
        }
        
        return division(somme, tableaux.length);
        
        } else {
            throw new ArithmeticException("Math Error!!");
        }
    }
}
