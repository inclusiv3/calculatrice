/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculatrice;

import com.mycompany.services.*;
import java.util.Scanner;

/**
 *
 * @author adrie
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("------Calculatrice - Menu------");
        System.out.println("1. Addition");
        System.out.println("2. Soustraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");
        System.out.println("5. Moyenne d'un tableaux");
        System.out.println("-------------------------------");
        System.out.print("Choisissez un type de calcul : ");
        Scanner scan = new Scanner(System.in);
        int choix = scan.nextInt();
        
        switch(choix){
            case 1:
                System.out.print("Entrer le premier nombre: ");
                double nombreUn = scan.nextDouble();
                System.out.print("Entrer le deuxieme nombre: ");
                double nombreDeux = scan.nextDouble();
                Calculatrice addition = new Calculatrice();
                System.out.println("L'addition: "+addition.addition(nombreUn,nombreDeux));
                break;
                
            case 2:
                System.out.print("Entrer le premier nombre: ");
                nombreUn = scan.nextDouble();
                System.out.print("Entrer le deuxieme nombre: ");
                nombreDeux = scan.nextDouble();
                Calculatrice soustraction = new Calculatrice();
                System.out.println("Soustraction : "+soustraction.soustraction(nombreUn,nombreDeux));
                break;
                
            case 3:
                System.out.print("Entrer le premier nombre: ");
                nombreUn = scan.nextDouble();
                System.out.print("Entrer le deuxieme nombre: ");
                nombreDeux = scan.nextDouble();
                Calculatrice multiplication = new Calculatrice();
                System.out.println("Soustraction : "+multiplication.multiplication(nombreUn,nombreDeux));
                break;
                
            case 4:
                System.out.print("Entrer le premier nombre: ");
                nombreUn = scan.nextDouble();
                System.out.print("Entrer le deuxieme nombre: ");
                nombreDeux = scan.nextDouble();
                Calculatrice division = new Calculatrice();
                System.out.println("Division : "+division.division(nombreUn,nombreDeux));
                break;
                
            case 5:
                System.out.print("Donner le longueur de tableaux : ");
                int longueur = scan.nextInt();
                int[] tableaux = new int[longueur];
                for(int i=0; i<longueur; i++){
                    System.out.print("Entrer l'element en "+i+1+" position : ");
                    tableaux[i] = scan.nextInt();
                }
                Calculatrice moyenne = new Calculatrice();
                System.out.println("Moyenne : "+moyenne.moyenne(tableaux));
                break;
                
            default:
                System.out.println("Mauvaix choix");
        }
        System.out.println("-------------------------------");
    }
}
