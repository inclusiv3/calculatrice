/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.services;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author adrie
 */
public class CalculatriceIT {
    
    public CalculatriceIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addition method, of class Calculatrice.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        double nombreUn = 7;
        double nombreDeux = 15;
        Calculatrice instance = new Calculatrice();
        double expResult = 22;
        double result = instance.addition(nombreUn, nombreDeux);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @Test
    public void testSoustraction() {
        System.out.println("soustraction");
        double nombreUn = 33;
        double nombreDeux = 11;
        Calculatrice instance = new Calculatrice();
        double expResult = 22;
        double result = instance.soustraction(nombreUn, nombreDeux);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of multiplication method, of class Calculatrice.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        double nombreUn = 32;
        double nombreDeux = 4;
        Calculatrice instance = new Calculatrice();
        double expResult = 128;
        double result = instance.multiplication(nombreUn, nombreDeux);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of division method, of class Calculatrice.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double numerateur = 32;
        double diviseur = 8;
        Calculatrice instance = new Calculatrice();
        double expResult = 4;
        double result = instance.division(numerateur, diviseur);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of moyenne method, of class Calculatrice.
     */
    @Test
    public void testMoyenne() {
        System.out.println("moyenne");
        int[] tableaux = {4,8,7,6,8,7,9};
        Calculatrice instance = new Calculatrice();
        double expResult = 7;
        double result = instance.moyenne(tableaux);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
